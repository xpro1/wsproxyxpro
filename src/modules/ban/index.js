// Logs
var mes = require('../../message');

let connected_list = {};

function checkAllowed(req, next) {
	var target = req.url.substr(1);
	var from   = req.connection.remoteAddress;
	let connect = false;

	if (connected_list[from] === undefined) {
		connected_list[from] = { banned: false };
	}

	if (connected_list[from][target] === undefined) {
		connected_list[from][target] = [];
	}

	if (connected_list[from].banned) {
		//  5 min ban
		if (connected_list[from].banned_tick + 300000 > Date.now())
			mes.warn("Reject requested connection from '%s' to '%s'. Banned because of too many request.", from, target);
		else {
			connected_list[from].banned = false;
		}
	}

	if (connected_list[from][target].length > 0) {
		connected_list[from][target] = connected_list[from][target].filter(l => l.tick + 60000 > Date.now());
	}

	if (!connected_list[from].banned) {
		if (connected_list[from][target].length >= 5) {
			mes.warn("Reject requested connection from '%s' to '%s'. Too many requests '%s'.", from, target, connected_list[from][target].length);
			connected_list[from].banned = true;
			connected_list[from].banned_tick = Date.now();
		} else {
			connect = true;
		}

		let entry = { tick: Date.now() };
		connected_list[from][target].push(entry);
		
	}

	next(connect);
}


// Exports methods
module.exports = {
	verify: checkAllowed //module.verify method
}
