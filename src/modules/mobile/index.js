// Logs
var mes = require('../../message');
var MobileDetect = require('mobile-detect');


function checkAllowed(req, next) {
	let target = req.url.substr(1);
	let from   = req.connection.remoteAddress;
	let md = new MobileDetect(req.headers['user-agent']);

	if(md.mobile() == null) {
		mes.warn("Reject requested connection from '%s' to '%s' [NOT A MOBILE].", from, target);
		next(false);
	} else {
		mes.info("Mobile %s accepted from '%s' to '%s'", md.phone(), from, target);
		next(true);
	}
}


// Exports methods
module.exports = {
	verify: checkAllowed //module.verify method
}
