/**
 * Dependencies
 */
var http = require('http');
var https = require('https');
var fs = require('fs');
var ws = require('ws');
var modules = require('./modules');
var mes = require('./message');


/**
 * Proxy constructor
 */
var Proxy = require('./proxy');


/**
 * Initiate a server
 */
var Server = function Init(config) {
	let opts = {
		clientTracking: false,
		noServer: true
	}
	let server;

	mes.level = config.log;

	console.log("Log Level : ", mes.level);

	if (config.ssl) {
		server = https.createServer({
			key: fs.readFileSync(config.key),
			cert: fs.readFileSync(config.cert),
		}, function (req, res) {
			res.writeHead(200);
			res.end("Secure wsProxy running.\n");
		});

		server.listen(config.port)

		mes.status("Start a secure wsProxy on port %s", config.port)
	}
	else {
		server = http.createServer(function (req, res) {
			res.writeHead(200);
			res.end("wsProxy running.\n");
		});

		server.listen(config.port)

		mes.status("Start wsProxy on port %s", config.port)
	}

	var WebSocketServer = new ws.Server(opts)
	WebSocketServer.on('connection', onConnection);

	server.on('upgrade', async function upgrade(request, socket, head) {
		
		try {
			await onRequestConnect(request);
		} catch (e) {
			socket.destroy();
			return;
		}

		WebSocketServer.handleUpgrade(request, socket, head, function done(ws) {
			WebSocketServer.emit('connection', ws, request);
		});

	});

	return this;
}


/**
 * Before estabilishing a connection
 */
async function onRequestConnect(info) {

	return new Promise((resolve, reject) => {
		// Once we get a response from our modules, pass it through
		modules.method.verify(info, function (res) {
			if(res) resolve();
			else reject();
		})
	});

}


/**
 * Connection passed through verify, lets initiate a proxy
 */
function onConnection(ws, req) {

	ws.upgradeReq = req;

	modules.method.connect(ws, function (res) {
		//All modules have processed the connection, lets start the proxy
		new Proxy(ws);
	})

}


/**
 * Exports
 */
module.exports = Server;
